FROM openjdk:11-jdk-alpine
RUN mkdir -p /app/lib && mkdir -p /app/META-INF && mkdir -p /target/dependency
COPY build/blog-service-*.jar /build/blog-service.jar
RUN (cd /target/dependency; jar -xvf ../../build/blog-service.jar)
RUN cp -r /target/dependency/BOOT-INF/lib/* /app/lib
RUN cp -r /target/dependency/BOOT-INF/classes/* /app
RUN cp -r /target/dependency/META-INF/* /app/META-INF
RUN rm -rf /target/dependency
ENTRYPOINT java $JAVA_OPT -cp app:app/lib/* com.blog.blogservice.BlogServiceApplication
