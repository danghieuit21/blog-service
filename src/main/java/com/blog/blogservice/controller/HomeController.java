package com.blog.blogservice.controller;


import com.blog.blogservice.constants.UrlConstant;
import com.blog.blogservice.model.request.GetPostRequest;
import com.blog.blogservice.model.response.GetPostResponse;
import com.blog.blogservice.service.BlogService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(UrlConstant.BASE_URL)
@Validated
@RestController
public class HomeController {

  private final BlogService blogService;

  public HomeController(BlogService blogService) {
    this.blogService = blogService;
  }

  @GetMapping
  public ResponseEntity<GetPostResponse> getPost(
      @RequestParam(name = "key" ,required = false) String key,
      @RequestParam(name = "post_type", required = false) String postType,
      @RequestParam(name = "page", defaultValue = "0", required = false) int page,
      @RequestParam(name = "size", defaultValue = "10", required = false) int size) {
    GetPostRequest request = new GetPostRequest(key, postType, page, size);
  return blogService.getPost(request);
  }
}
