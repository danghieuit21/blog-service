package com.blog.blogservice.repository;

import com.blog.blogservice.entities.Tag;
import com.blog.blogservice.entities.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

  @Query("select new com.blog.blogservice.entities.Tag(t.code,t.name) "
      + "FROM Tag t INNER JOIN PostTag  pt ON t.id = pt.postId "
      + "WHERE pt.postId = ?1")
  List<Tag> getCustomByPostId(long postId);

}
