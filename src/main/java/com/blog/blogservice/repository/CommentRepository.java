package com.blog.blogservice.repository;

import com.blog.blogservice.entities.Comment;
import com.blog.blogservice.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

  long countAllByPostId(Long postId);
}
