package com.blog.blogservice.repository;

import com.blog.blogservice.entities.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

  Page<Post> findAllByTitleContainsOrContentContains(String title, String content, Pageable pageable);
  Page<Post> findAll(Pageable pageable);

}
