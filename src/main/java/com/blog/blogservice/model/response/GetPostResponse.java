package com.blog.blogservice.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GetPostResponse {

  private List<Post> posts;

  @Data
  @NoArgsConstructor
  public static class Post {

    private Long id;
    private String title;
    private Long createDate;
    private UserInfo userInfo;
    private String brief;
    private Long timeRead;
    private List<Tag> tags;
    private long totalComment;
    @JsonInclude()
    private String createdBy;

  }

  @Data
  @NoArgsConstructor
  public static class UserInfo {

    private String fullName;
    private String avatarImage;
  }

  @Data
  @NoArgsConstructor
  public static class Tag {

    private String name;
    private String code;
  }
}
