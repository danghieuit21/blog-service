package com.blog.blogservice.model.response;

import java.util.List;
import lombok.Data;

@Data
public class HeaderResponse {

  private List<HeaderTop> headerTop;

  @Data
  public static class HeaderTop {

    private String title;
    private String description;
    private String poster;
    private String image;
  }
}
