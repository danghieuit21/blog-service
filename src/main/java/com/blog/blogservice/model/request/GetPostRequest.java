package com.blog.blogservice.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetPostRequest {
  private String key;
  private String postType;
  private int page;
  private int size;
}
