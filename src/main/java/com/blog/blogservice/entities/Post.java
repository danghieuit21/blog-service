package com.blog.blogservice.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "posts")
public class Post extends AbstractAuditModel {

  @Column(name = "title")
  private String title;

  @Column(name = "image")
  private String iamge;

  @Column(name = "brief")
  private String brief;

  @Column(name = "content")
  private String content;

  @Column(name = "slug")
  private String slug;

  @Column(name = "status")
  private String status;

  @Column(name = "count_view")
  private Long countView;

  @Column(name = "hot")
  private Long hot;

  @Column(name = "time_read")
  private Integer timeRead;

}
