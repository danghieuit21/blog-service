package com.blog.blogservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "users")
@Data
public class User extends AbstractAuditModel {

    private static final long serialVersionUID = -5071893587064744309L;

    @Column(name = "full_name")
    private Long fullName;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "is_lock")
    private boolean isLock;

    @Column(name = "avatar_image")
    private String avatarImage;

    @Column(name = "status")
    private String status;
}
