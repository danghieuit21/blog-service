package com.blog.blogservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "comments")
public class Comment extends AbstractAuditModel{

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "content")
  private String content;

  @Column(name = "parent_id")
  private Long parentId;

  @Column(name = "post_id")
  private Long postId;

  @Column(name = "status")
  private String status;

}
