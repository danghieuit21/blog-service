package com.blog.blogservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "post_tags")
public class PostTag extends AbstractAuditModel{

  @Column(name = "tag_id")
  private Long tagId;

  @Column(name = "post_id")
  private Long postId;
}
