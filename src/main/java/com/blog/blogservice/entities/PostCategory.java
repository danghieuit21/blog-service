package com.blog.blogservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "post_categories")
public class PostCategory extends AbstractAuditModel{

  @Column(name = "post_id")
  private Long postId;

  @Column(name = "categoryId")
  private Long categoryId;
}
