package com.blog.blogservice.entities;

import java.security.SecureRandom;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "categorys")
public class Category extends AbstractAuditModel{

  @Column(name = "name")
  private String name;

  @Column(name = "code")
  private String code;

  @Column(name = "parent_id")
  private Long parentId;

  @Column(name = "slug")
  private String slug;

  @Column(name = "photo")
  private String photo;
}
