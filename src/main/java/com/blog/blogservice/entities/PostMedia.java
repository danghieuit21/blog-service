package com.blog.blogservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "post_medias")
public class PostMedia extends AbstractAuditModel{

  @Column(name = "owner_id")
  private Long ownerId;

  @Column(name = "link")
  private String link;

  @Column(name = "type")
  private String type;
}
