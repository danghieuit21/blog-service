package com.blog.blogservice.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

public class PageableBuilder {
    private PageableBuilder() {
    }

    public static Pageable getPageable(Integer page, Integer pageSize, Map<Integer, String> sortMap, List<String> sortCriteria) {
        if (CollectionUtils.isEmpty(sortCriteria)) {
            return PageRequest.of(page, pageSize);
        }

        List<Sort.Order> orders = new ArrayList<>();
        for (String sort : sortCriteria) {
            String[] split = sort.split(",");
            Sort.Direction direction;
            if (split.length == 1) {
                direction = Sort.Direction.ASC;
            } else {
                direction = getSortDirection(split[1]);
            }
            orders.add(new Sort.Order(direction, sortMap.get(Integer.parseInt(split[0]))));
        }

        return PageRequest.of(page, pageSize, Sort.by(orders));
    }

    private static Sort.Direction getSortDirection(String direction) {
        try {
            return Sort.Direction.valueOf(direction);
        } catch (Exception e) {
            return Sort.Direction.ASC;
        }
    }
}
