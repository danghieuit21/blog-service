package com.blog.blogservice.mapper;

import com.blog.blogservice.entities.User;
import com.blog.blogservice.model.response.GetPostResponse;

import com.blog.blogservice.model.response.GetPostResponse.Post;
import com.blog.blogservice.model.response.GetPostResponse.Tag;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class BlogMapper {
    public static final BlogMapper INSTANCE = Mappers.getMapper(BlogMapper.class);

    public abstract List<Post> listEntityToListModelPost(List<com.blog.blogservice.entities.Post> entity);
    public abstract List<Tag> listEntityToListModeTag(List<com.blog.blogservice.entities.Tag> entity);
    public abstract GetPostResponse.UserInfo entityToModel(User user);
}
