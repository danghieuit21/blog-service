package com.blog.blogservice.service;

import com.blog.blogservice.model.request.GetPostRequest;
import com.blog.blogservice.model.response.GetPostResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface BlogService {
  ResponseEntity<GetPostResponse> getPost(GetPostRequest request);
}
