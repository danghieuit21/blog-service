package com.blog.blogservice.service.impl;

import com.blog.blogservice.entities.Post;
import com.blog.blogservice.entities.User;
import com.blog.blogservice.mapper.BlogMapper;
import com.blog.blogservice.model.request.GetPostRequest;
import com.blog.blogservice.model.response.GetPostResponse;
import com.blog.blogservice.repository.CommentRepository;
import com.blog.blogservice.repository.PostRepository;
import com.blog.blogservice.repository.TagRepository;
import com.blog.blogservice.repository.UserRepository;
import com.blog.blogservice.service.BlogService;
import com.blog.blogservice.utils.PageableBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Service
public class BlogServiceImpl implements BlogService {

  private final PostRepository postRepository;
  private final UserRepository userRepository;
  private final CommentRepository commentRepository;
  private final TagRepository tagRepository;

  private static Map<Integer, String> filterSortMap = new HashMap<>();

  static {
    filterSortMap.put(0, "createdTime");
    filterSortMap.put(1, "countView");
    filterSortMap.put(2, "hot");
  }

  public BlogServiceImpl(PostRepository postRepository, UserRepository userRepository, CommentRepository commentRepository, TagRepository tagRepository) {
    this.postRepository = postRepository;
    this.userRepository = userRepository;
    this.commentRepository = commentRepository;
    this.tagRepository = tagRepository;
  }

  @Override
  public ResponseEntity<GetPostResponse> getPost(GetPostRequest request) {
    int page = request.getPage();
    int size = request.getSize();
    if (page > 0) {
      page = page - 1;
    } else {
      page = 0;
    }
    List<String> sortBy;
    if (StringUtils.isEmpty(request.getPostType())) {
      sortBy = List.of("0,DESC");
    } else {
      sortBy = List.of(request.getPostType());
    }
    Pageable pageable = PageableBuilder.getPageable(page, size, filterSortMap, sortBy);
    Page<Post> pagePost;
    if (StringUtils.isEmpty(request.getKey())) {
      pagePost = postRepository.findAll(pageable);
    } else {
      pagePost = postRepository.findAllByTitleContainsOrContentContains(request.getKey(), request.getKey(), pageable);
    }
    GetPostResponse response = new GetPostResponse();
    BlogMapper mapper = BlogMapper.INSTANCE;
    response.setPosts(mapper.listEntityToListModelPost(pagePost.getContent()));

    response.getPosts().forEach(post -> {
      User user = userRepository.findByUserName(post.getCreatedBy());
      if (user != null)
        post.setUserInfo(mapper.entityToModel(user));

      post.setTotalComment(commentRepository.countAllByPostId(post.getId()));
      post.setTags(mapper.listEntityToListModeTag(tagRepository.getCustomByPostId(post.getId())));
    });

    return ResponseEntity.ok(response);
  }
}
